<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StudentInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('student_information', function (Blueprint $table) {
            $table->id();
            $table->string('student_name');
            $table->string('grade')->nullable();
			$table->text('image')->nullable();
			$table->date('dob');
			$table->text('address')->nullable();
			$table->string('city')->nullable();
			$table->string('country')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('flights');

    }
}
