@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Student Information</h2>
            </div>
            
        </div>
    </div>
@if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message }}</p>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Error!</strong> 
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

	<form method="POST" enctype='multipart/form-data' action="{{ route('students.store') }}" accept-charset="UTF-8" >
        @csrf

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="student_name" class="form-control" placeholder="Name">
                </div>
            </div>
			
			 <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Grade:</strong>
                    <input type="text" name="grade" class="form-control" placeholder="A B">
                </div>
            </div>
			 <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>DOB:</strong>
                 <input type="date" name="dob" id="date" class="form-control" style="width: 100%; display: inline;">

                </div>
            </div>
			 <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Upload Image:</strong>
                    <input type="file" name="image" class="form-control" placeholder="">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Address:</strong>
                    <textarea class="form-control" style="height:50px" name="address"
                        placeholder="address"></textarea>
                </div>
            </div>
			 <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>City:</strong>
                    <input type="text" name="city" class="form-control" placeholder="city">
                </div>
            </div>
			 <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Coutry:</strong>
                    <input type="text" name="country" class="form-control" placeholder="country">
                </div>
            </div>
			
           
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection