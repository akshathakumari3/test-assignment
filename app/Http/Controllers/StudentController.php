<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use DataTables;
use Session;
use Illuminate\Support\Facades\Log;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = Student::select('*');

            return Datatables::of($data)

                    ->addIndexColumn()

                    ->addColumn('dob', function($row){

     

                           $dob = date('d-M-Y',strtotime($row->dob));

       

                            return $dob;

                    })

                    ->rawColumns(['dob'])
					->filter(function($row) use ($request) {
                   if ($request->has('search.value')) {
                        $row->where(function($q) use ($request) {

                                $keyword = $request->input('search.value');
                                  
                                    $dateformat = date('Y-m-d',strtotime($keyword));;
                                    $q->orWhereRaw("student_name like ?", ["%{$keyword}%"]);
                                    $q->orWhereRaw("grade like ?",["%{$keyword}%"]);
                                    $q->orWhereRaw("dob like ?",["%{$dateformat}%"]);
                        });
                    }
            })
                    ->make(true);

        }

        

        return view('student');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		return view('create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		try {	
		 request()->validate([
         'student_name' => 'required',
          'grade' => 'required',
          'dob' => 'required',
		  'address' => 'required',
		   'city' => 'required',
		  'country' => 'required',
		  'image' => 'mimes:jpeg,jpg,png,gif|max:10000'
    ],
    [
        'student_name.required' => 'Student name Field is Required',
        'grade.required' => 'Grade is required!',
		'dob.required' => 'Date of birth is required!',
		'address.required' => 'Address is required!',
		'city.required' => 'City is required!',
		'country.required' => 'Country is required!',
    ]);
	$image_path='';
	 if($request->image){
		   log::info("Image is attached");
		$file = $request->file('image');
 
		$format = $request->image->extension();
        //save full adress of image
		$image_path = $request->image->store('images');

        $name = $file->getClientOriginalName();
		 }
		$student						= new Student();
		$student->student_name			=$request->student_name;
		$student->grade					=$request->grade;
		$student->dob					=$request->dob;
		$student->address				=$request->address;
		$student->city					=$request->city;
		$student->country				=$request->country;
		$student->image					=$image_path;
		$student->save();
		  log::info("Student Information created successfully.");
		return redirect()->route('students.create')
            ->with('success', 'Student Information created successfully.');
		}
		catch(Exception $e)  {
				return abort(500, "Duplicate customer");			
		}
	
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }
}
