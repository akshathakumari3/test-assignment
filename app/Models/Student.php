<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class Student extends Model
{
    
    public $table = 'student_information';
    public $fillable = ['student_name','grade','image','dob','address','city','country'];

    /**
     * Validation rules
     *
     * @var array
     */
  
    


}
